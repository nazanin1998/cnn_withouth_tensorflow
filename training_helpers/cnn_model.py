from training_helpers.convolution_2d import Conv2D
from training_helpers.dense import Dense
from training_helpers.drop_out import DropOutClass
from training_helpers.pooling_2d import Pooling2D
from training_helpers.activation_functions import ActivationFunction
from training_helpers.flatten import Flatten
from training_helpers.input_2d import Input2DClass
import numpy


class Model:  # create a CNN model

    def __init__(self, last_layer, epochs=10, learning_rate=0.01):

        self.last_layer = last_layer
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.network_layers = self.get_layers()
        self.summary()

    def get_layers(self):
        """ Returns a list of all layers in the CNN model """
        network_layers = []
        layer = self.last_layer

        while "previous_layer" in layer.__init__.__code__.co_varnames:
            network_layers.insert(0, layer)
            layer = layer.previous_layer

        return network_layers

    def train(self, train_inputs, train_outputs):

        if train_inputs.ndim != 4:
            raise ValueError("The training data input must have 4 dimensions")

        if train_inputs.shape[0] != len(train_outputs):
            raise ValueError("Mismatch between the number of input samples and number of labels")
        network_predictions = []
        network_error = 0

        for epoch in range(self.epochs):
            print("Epoch {epoch}".format(epoch=epoch))
            for row in range(train_inputs.shape[0]):
                self.feed_sample(train_inputs[row, :])
                try:
                    predicted_label = \
                        numpy.where(numpy.max(self.last_layer.layer_output) == self.last_layer.layer_output)[0][0]
                except IndexError:
                    print(self.last_layer.layer_output)
                    raise IndexError("Index out of range")
                network_predictions.append(predicted_label)

                network_error = network_error + abs(predicted_label - train_outputs[row])

            self.update_weights(network_error)

    def feed_sample(self, sample):

        """
        Feeds a sample in the CNN layers.

        sample: The samples to be fed to the CNN layers.

        Returns results of the last layer in the CNN.
        """

        last_layer_outputs = sample
        for layer in self.network_layers:
            if type(layer) is Conv2D:
                #                import time
                #                time1 = time.time()
                layer.conv(input2D=last_layer_outputs)
            #                time2 = time.time()
            #                print(time2 - time1)
            elif type(layer) is Dense:
                layer.dense_layer(layer_input=last_layer_outputs)
            elif type(layer) is Pooling2D and layer.is_max_pooling:
                layer.max_pooling(input2D=last_layer_outputs)
            elif type(layer) is Pooling2D and not layer.is_max_pooling:
                layer.average_pooling(input2D=last_layer_outputs)
            elif type(layer) is ActivationFunction:
                layer.relu_layer(layer_input=last_layer_outputs)
            elif type(layer) is ActivationFunction:
                layer.sigmoid_layer(layer_input=last_layer_outputs)
            elif type(layer) is Flatten:
                layer.flatten(input2D=last_layer_outputs)
            elif type(layer) is Input2DClass:
                pass
            elif type(layer) is DropOutClass:
                layer.dropout(input2D=last_layer_outputs)
            else:
                print("Other")
                raise TypeError("The layer of type {layer_type} is not supported yet.".format(layer_type=type(layer)))

            last_layer_outputs = layer.layer_output
        return self.network_layers[-1].layer_output

    def update_weights(self, network_error):

        """
        Updates the weights of the CNN.
        It is important to note that no learning algorithm is used for training the CNN.
         Just the learning rate is used for making some changes which is better than leaving the weights unchanged.

        This method loops through the layers and updates their weights.
        network_error: The network error in the last epoch.
        """

        for layer in self.network_layers:
            if "trained_weights" in vars(layer).keys():
                layer.trained_weights = layer.trained_weights - network_error * self.learning_rate * layer.trained_weights

    def predict(self, data_inputs):

        """
        Uses the trained CNN for making predictions.

        data_inputs: The inputs to predict their label.
        Returns a list holding the samples predictions.
        """

        if data_inputs.ndim != 4:
            raise ValueError(
                "The data input has {num_dims} but it must have 4 dimensions. The first dimension is the number of training samples, the second & third dimensions represent the width and height of the sample, and the fourth dimension represents the number of channels in the sample.".format(
                    num_dims=data_inputs.ndim))

        predictions = []
        for sample in data_inputs:
            probs = self.feed_sample(sample=sample)
            predicted_label = numpy.where(numpy.max(probs) == probs)[0][0]
            predictions.append(predicted_label)
        return predictions

    def summary(self):
        print("\n----------Network Architecture----------")
        for layer in self.network_layers:
            print(type(layer))
        print("----------------------------------------\n")
