import random


class DropOutClass:

    def __init__(self, previous_layer, percentage):
        if previous_layer is None:
            raise TypeError("The previous layer cannot be of Type 'None'.")
        self.previous_layer = previous_layer
        self.percentage = percentage
        self.layer_input_size = self.previous_layer.layer_output_size
        self.layer_output_size = self.get_output_shape()
        self.layer_output = None
        self.summery()

    def get_output_shape(self):
        percent = (1 - self.percentage)

        second = int(self.layer_input_size[1] * percent)
        return (
            self.layer_input_size[0], second, self.layer_input_size[2])

    def summery(self):
        print("**  THIRD_STEP ==> DROPOUT **\n\t\tProperties ==> input_size = " + str(
            self.layer_input_size) + ",\t" +
              "output_size = " + str(self.layer_output_size) + ",\t" +
              "output = " + str(self.layer_output) + ",\t" +
              "percentage = " + str(self.percentage))

    def dropout(self, input2D):
        row = input2D.shape[0]
        col = input2D.shape[1]
        percent = (1 - self.percentage)
        active_input_nodes = int(col * percent)
        active_input_indices = sorted(random.sample(range(0, col),
                                                    active_input_nodes))
        self.layer_output = input2D[:, active_input_indices]
