import numpy
import pandas as pd


class Input2DClass:  # input_shape: Shape of the input sample to the CNN.

    def __init__(self, X, Y, dataset=2):

        self.X = X
        self.Y = Y
        self._read_data(dataset)
        self._dimension_check()

    def get_test_inputs(self):
        return self.x_test

    def get_test_outputs(self):
        return self.y_test

    def get_train_inputs(self):
        return self._train_inputs

    def get_train_outputs(self):
        return self._train_outputs

    def _read_data_from_csv_file(self):
        train_path = "./datasets/dataset_1/train.csv"
        test_path = "./datasets/dataset_1/test.csv"
        y_test_path = "./datasets/dataset_1/sample_submission.csv"
        train = pd.read_csv(train_path)  # read train
        x_test = pd.read_csv(test_path)  # read test
        y_test = pd.read_csv(y_test_path)  # read test
        self._train_outputs = train["label"]  # put labels into y_train variable

        self._train_inputs = train.drop(labels=["label"], axis=1)  # Drop 'label' column

        self._train_inputs = self._train_inputs.values.reshape(-1, self.X, self.Y, 1)
        self.x_test = x_test.values.reshape(-1, self.X, self.Y, 1)
        self.y_test = y_test["Label"]

    def _read_data(self, datasets):
        print("1 ==> (FIRST_STEP) ==> Read train inputs and outputs. **")
        if datasets == 1:
            self._read_data_from_csv_file()
        else:
            self._read_data2()
        print("\tX_train.shape is : " + str(self._train_inputs.shape))
        print("\tY_train.shape is : " + str(self._train_outputs.shape))

    def _read_data2(self):
        train_path = "./datasets/dataset_2/dataset_inputs.npy"
        y_train_path = "./datasets/dataset_2/dataset_outputs.npy"
        self._train_inputs = numpy.load(train_path)
        self._train_outputs = numpy.load(y_train_path)
        self.x_test = self._train_inputs
        self.y_test = self._train_outputs

    def _dimension_check(self):
        sample_shape = self._train_inputs.shape[1:]

        if len(sample_shape) < 2:  # If the sample dimensions < 2 ==> EXCEPTION
            raise ValueError("input dimensions could not be less than 2")

        elif len(sample_shape) == 2:  # If the sample dimensions == 2 ==> third dimension is set to 1
            sample_shape = (sample_shape[0], sample_shape[1], 1)

        for dimension_index, dimension in enumerate(sample_shape):
            if dimension <= 0:
                raise ValueError("The dimension size of the inputs cannot be <= 0")
        print("\tsample_shape is : " + str(sample_shape))
        self.input_shape = sample_shape
        self.layer_output_size = sample_shape
