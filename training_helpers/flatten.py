import functools

import numpy

class Flatten:

    def __init__(self, previous_layer):
        if previous_layer is None:
            raise TypeError("The previous layer cannot be of Type 'None'.")
        self.previous_layer = previous_layer
        self.layer_input_size = self.previous_layer.layer_output_size
        self.layer_output_size = functools.reduce(lambda x, y: x * y, self.previous_layer.layer_output_size)
        self.layer_output = None
        self.summery()

    def summery(self):
        print("**  THIRD_STEP ==> FLATTENING **\n\t\tProperties ==> input_size = " + str(
            self.layer_input_size) + ",\t" +
              "output_size = " + str(self.layer_output_size) + ",\t" +
              "output = " + str(self.layer_output))

    def flatten(self, input2D):
        self.layer_output_size = input2D.size
        self.layer_output = numpy.ravel(input2D)
