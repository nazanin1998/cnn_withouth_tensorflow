import numpy


class ActivationFunction:
    supported_activation_functions = ("sigmoid", "relu", "softmax")

    def __init__(self, previous_layer):
        # Supported activation functions by the cnn.py module.
        if previous_layer is None:
            raise TypeError("The previous layer cannot be of Type 'None'")
        self.previous_layer = previous_layer
        self.layer_input_size = self.previous_layer.layer_output_size
        self.layer_output_size = self.previous_layer.layer_output_size
        self.layer_output = None
        self.summery()

    def summery(self):
        print("\t\tActivation ==> input_size = " + str(self.layer_input_size) + ",\t" +
              "output_size = " + str(self.layer_output_size) + ",\t" +
              "output = " + str(self.layer_output) + ",\t")

    def sigmoid_layer(self, layer_input):
        self.layer_output_size = layer_input.size
        self.layer_output = ActivationFunction.sigmoid(layer_input)

    def relu_layer(self, layer_input):
        self.layer_output_size = layer_input.size
        self.layer_output = ActivationFunction.relu(layer_input)

    @staticmethod
    def sigmoid(input_param):
        if type(input_param) in [list, tuple]:
            input_param = numpy.array(input_param)
        return 1.0 / (1 + numpy.exp(-1 * input_param))

    @staticmethod
    def softmax(layer_outputs):
        return layer_outputs / (numpy.sum(layer_outputs) + 0.000001)

    @staticmethod
    def relu(input_param):
        if not (type(input_param) in [list, tuple, numpy.ndarray]):
            if input_param < 0:
                return 0
            else:
                return input_param
        elif type(input_param) in [list, tuple]:
            input_param = numpy.array(input_param)

        result = input_param
        result[input_param < 0] = 0

        return result
