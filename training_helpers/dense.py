import numpy
from training_helpers.activation_functions import ActivationFunction


class Dense:  # fully connected

    def __init__(self, num_neurons, previous_layer, activation_function="relu"):
        if num_neurons <= 0:
            raise ValueError("Number of neurons cannot be <= 0")
        self.num_neurons = num_neurons
        self._validate_activation(activation_function)

        if previous_layer is None:
            raise TypeError("The previous layer cannot be of Type 'None'")
        self.previous_layer = previous_layer

        if type(self.previous_layer.layer_output_size) in [list, tuple, numpy.ndarray] and len(
                self.previous_layer.layer_output_size) > 1:
            raise ValueError("The input to the dense layer must be of type int but {sh} found.".format(
                sh=type(self.previous_layer.layer_output_size)))
        # Initializing the weights of the layer.
        self.initial_weights = numpy.random.uniform(low=-0.5,
                                                    high=0.5,
                                                    size=(self.previous_layer.layer_output_size, self.num_neurons))
        # Just initialized to be equal to the initial weights
        self.trained_weights = self.initial_weights.copy()
        self.layer_input_size = self.previous_layer.layer_output_size
        self.layer_output_size = num_neurons
        self.layer_output = None
        self.summery()

    def summery(self):
        print("\t\tDense      ==> input_size = " + str(self.layer_input_size) + ",\t" +
              "output_size = " + str(self.layer_output_size) + ",\t" +
              "output = " + str(self.layer_output) + ",\t" +
              "activation = " + str(self.activation) + ",\t")

    def _validate_activation(self, activation_function):
        if activation_function == "relu":
            self.activation = ActivationFunction.relu
        elif activation_function == "sigmoid":
            self.activation = ActivationFunction.sigmoid
        elif activation_function == "softmax":
            self.activation = ActivationFunction.softmax
        else:
            raise ValueError(
                "The specified activation function '{activation_function}' is not among the supported activation functions {supported_activation_functions}. Please use one of the supported functions.".format(
                    activation_function=activation_function,
                    supported_activation_functions=ActivationFunction('').supported_activation_functions))

        self.activation_function = activation_function

    def dense_layer(self, layer_input):

        """
        Calculates the output of the dense layer.

        layer_input: The input to the dense layer
        The dense_layer() method saves its result in the layer_output attribute.
        """

        if self.trained_weights is None:
            raise TypeError("The weights of the dense layer cannot be of Type 'None'.")

        sop = numpy.matmul(layer_input, self.trained_weights)

        self.layer_output = self.activation(sop)
