import numpy
from training_helpers.activation_functions import ActivationFunction


class Conv2D:

    def __init__(self, num_filters, kernel_size, previous_layer, activation_function=None):

        self._validate_filter(num_filters, kernel_size)
        self._validate_activation(activation_function)
        self._validate_previous_layer(previous_layer)

        self.initial_weights = self._make_filter()
        self.trained_weights = self.initial_weights.copy()
        self.layer_input_size = self.previous_layer.layer_output_size

        # It must consider strides and paddings
        self.layer_output_size = (self.previous_layer.layer_output_size[0] - self.kernel_size + 1,
                                  self.previous_layer.layer_output_size[1] - self.kernel_size + 1,
                                  num_filters)
        self.layer_output = None
        self.summery()

    def summery(self):
        print("\t\tProperties ==> input_size = " + str(self.layer_input_size) + ",\t" +
              "output_size = " + str(self.layer_output_size) + ",\t" +
              "output = " + str(self.layer_output) + ",\t" +
              "num_filters = " + str(self.num_filters) + ",\t" +
              "kernel_size = " + str(self.kernel_size) + ",\t" +
              "activation = " + str(self.activation_function))

    def _make_filter(self):
        self.filter_bank_size = (self.num_filters,
                                 self.kernel_size,
                                 self.kernel_size,
                                 self.previous_layer.layer_output_size[-1])
        # Initializing the filters of the conv layer.
        return numpy.random.uniform(low=-0.5,
                                    high=0.5,
                                    size=self.filter_bank_size)

    def _validate_activation(self, activation_function):
        if activation_function is None:
            self.activation = None
        elif activation_function == "relu":
            self.activation = ActivationFunction.relu
        elif activation_function == "sigmoid":
            self.activation = ActivationFunction.sigmoid
        elif activation_function == "softmax":
            raise ValueError("The softmax activation function cannot be used in a conv layer.")
        else:
            raise ValueError("The specified activation function is not supported")
        self.activation_function = activation_function

    def _validate_filter(self, num_filters, kernel_size):
        if num_filters <= 0:
            raise ValueError("Number of filters cannot be <= 0")
        self.num_filters = num_filters

        if kernel_size <= 0:
            raise ValueError("The kernel size cannot be <= 0")
        self.kernel_size = kernel_size

    def _validate_previous_layer(self, previous_layer):
        if previous_layer is None:
            raise TypeError("The previous layer cannot be of Type 'None'")
        self.previous_layer = previous_layer

    def conv_(self, input2D, conv_filter):  # Convolves the input (input2D) by a single filter (conv_filter).

        result = numpy.zeros(shape=(input2D.shape[0], input2D.shape[1], conv_filter.shape[0]))
        # Looping through the image to apply the convolution operation.
        for r in numpy.uint16(numpy.arange(self.filter_bank_size[1] / 2.0,
                                           input2D.shape[0] - self.filter_bank_size[1] / 2.0 + 1)):
            for c in numpy.uint16(numpy.arange(self.filter_bank_size[1] / 2.0,
                                               input2D.shape[1] - self.filter_bank_size[1] / 2.0 + 1)):
                """
                Getting the current region to get multiplied with the filter.
                How to loop through the image and get the region based on 
                the image and filer sizes is the most tricky part of convolution.
                """
                curr_region = input2D[
                              r - numpy.uint16(numpy.floor(self.filter_bank_size[1] / 2.0)):r + numpy.uint16(
                                  numpy.ceil(self.filter_bank_size[1] / 2.0)),
                              c - numpy.uint16(numpy.floor(self.filter_bank_size[1] / 2.0)):c + numpy.uint16(
                                  numpy.ceil(self.filter_bank_size[1] / 2.0))]

                for filter_idx in range(conv_filter.shape[0]):
                    curr_result = curr_region * conv_filter[filter_idx]
                    conv_sum = numpy.sum(curr_result)  # Summing the result of multiplication.

                    if self.activation is None:
                        result[r, c, filter_idx] = conv_sum  # Saving the SOP in the convolution layer feature map.
                    else:
                        result[r, c, filter_idx] = self.activation(
                            conv_sum)  # Saving the activation function result in the convolution layer feature map.

        # Clipping the outliers of the result matrix.
        final_result = result[numpy.uint16(self.filter_bank_size[1] / 2.0):result.shape[0] - numpy.uint16(
            self.filter_bank_size[1] / 2.0),
                       numpy.uint16(self.filter_bank_size[1] / 2.0):result.shape[1] - numpy.uint16(
                           self.filter_bank_size[1] / 2.0), :]
        return final_result

    def conv(self, input2D):
        """
        Convolves the input (input2D) by a filter bank.

        input2D: The input to be convolved by the filter bank.
        The conv() method saves the result of convolving the input by the filter bank in the layer_output attribute.
        """

        if len(input2D.shape) != len(
                self.initial_weights.shape) - 1:  # Check if there is a match in the number of dimensions between the image and the filters.
            raise ValueError("Number of dimensions in the conv filter and the input do not match.")
        if len(input2D.shape) > 2 or len(
                self.initial_weights.shape) > 3:  # Check if number of image channels matches the filter depth.
            if input2D.shape[-1] != self.initial_weights.shape[-1]:
                raise ValueError("Number of channels in both the input and the filter must match.")
        if self.initial_weights.shape[1] != self.initial_weights.shape[2]:  # Check if filter dimensions are equal.
            raise ValueError('A filter must be a square matrix. I.e. number of rows and columns must match.')
        if self.initial_weights.shape[1] % 2 == 0:  # Check if filter diemnsions are odd.
            raise ValueError('A filter must have an odd size. I.e. number of rows and columns must be odd.')

        self.layer_output = self.conv_(input2D, self.trained_weights)
