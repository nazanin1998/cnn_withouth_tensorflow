import numpy


class Pooling2D:

    def __init__(self, pool_size, previous_layer, stride=2, is_max_pooling=True):

        if not (type(pool_size) is int):
            raise ValueError("The expected type of the pool_size is int but {pool_size_type} found.")

        if pool_size <= 0:
            raise ValueError("The passed value to the pool_size parameter cannot be <= 0.")
        self.pool_size = pool_size

        if stride <= 0:
            raise ValueError("The passed value to the stride parameter cannot be <= 0.")
        self.stride = stride

        if previous_layer is None:
            raise TypeError("The previous layer cannot be of Type 'None'")
        self.previous_layer = previous_layer

        self.layer_input_size = self.previous_layer.layer_output_size
        self.is_max_pooling = is_max_pooling
        self.layer_output_size = (
            numpy.uint16((self.previous_layer.layer_output_size[0] - self.pool_size + 1) / stride + 1),
            numpy.uint16((self.previous_layer.layer_output_size[1] - self.pool_size + 1) / stride + 1),
            self.previous_layer.layer_output_size[-1])
        self.layer_output = None
        self.summery()

    def summery(self):
        print("\t\tPooling    ==> input_size = " + str(self.layer_input_size) + ",\t" +
              "output_size = " + str(self.layer_output_size) + ",\t" +
              "output = " + str(self.layer_output) + ",\t" +
              "pool_size = " + str(self.pool_size) + ",\t" +
              "is_max_pool = " + str(self.is_max_pooling) + ",\t" +
              "stride = " + str(self.stride) + ",\t")

    def average_pooling(self, input2D):
        # Preparing the output of the pooling operation.
        pool_out = numpy.zeros((numpy.uint16((input2D.shape[0] - self.pool_size + 1) / self.stride + 1),
                                numpy.uint16((input2D.shape[1] - self.pool_size + 1) / self.stride + 1),
                                input2D.shape[-1]))
        for map_num in range(input2D.shape[-1]):
            r2 = 0
            for r in numpy.arange(0, input2D.shape[0] - self.pool_size + 1, self.stride):
                c2 = 0
                for c in numpy.arange(0, input2D.shape[1] - self.pool_size + 1, self.stride):
                    pool_out[r2, c2, map_num] = numpy.mean(
                        [input2D[r:r + self.pool_size, c:c + self.pool_size, map_num]])
                    c2 = c2 + 1
                r2 = r2 + 1

        self.layer_output = pool_out

    def max_pooling(self, input2D):
        # Preparing the output of the pooling operation.
        pool_out = numpy.zeros((numpy.uint16((input2D.shape[0] - self.pool_size + 1) / self.stride + 1),
                                numpy.uint16((input2D.shape[1] - self.pool_size + 1) / self.stride + 1),
                                input2D.shape[-1]))
        for map_num in range(input2D.shape[-1]):
            r2 = 0
            for r in numpy.arange(0, input2D.shape[0] - self.pool_size + 1, self.stride):
                c2 = 0
                for c in numpy.arange(0, input2D.shape[1] - self.pool_size + 1, self.stride):
                    pool_out[r2, c2, map_num] = numpy.max(
                        [input2D[r:r + self.pool_size, c:c + self.pool_size, map_num]])
                    c2 = c2 + 1
                r2 = r2 + 1

        self.layer_output = pool_out
