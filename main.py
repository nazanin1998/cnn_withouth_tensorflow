import numpy

from training_helpers.drop_out import DropOutClass
from training_helpers.input_2d import Input2DClass
from training_helpers.convolution_2d import Conv2D
from training_helpers.pooling_2d import Pooling2D
from training_helpers.flatten import Flatten
from training_helpers.activation_functions import ActivationFunction
from training_helpers.cnn_model import Model
from training_helpers.dense import Dense
from datetime import datetime

now = datetime.now()

current_time = now.strftime("%H:%M:%S")

print("Current Time is :", current_time)

pixel_x_size = 28
pixel_y_size = 28
datasets = 2
input_layer = Input2DClass(pixel_x_size, pixel_y_size, dataset=datasets)
num_classes = 0
if datasets == 2:
    num_classes = 4
else:
    num_classes = input_layer.get_train_outputs().value_counts().size

print("\n2 ==> (SECOND_STEP) ==> CONV LAYER")
print("\t1s conv layer")
#
conv_layer1 = Conv2D(num_filters=2,
                     kernel_size=3,
                     previous_layer=input_layer,
                     activation_function='relu')

activation_layer1 = ActivationFunction(previous_layer=conv_layer1)
average_pooling_layer = Pooling2D(pool_size=2,
                                  previous_layer=activation_layer1,
                                  stride=2)
# drop_layer_1 = DropOutClass(previous_layer=average_pooling_layer, percentage=0.25)
# dropout //todo

print("\t2s conv layer")
conv_layer2 = Conv2D(num_filters=3,
                     kernel_size=3,
                     previous_layer=average_pooling_layer,
                     activation_function='relu')
activation_layer2 = ActivationFunction(previous_layer=conv_layer2)
max_pooling_layer = Pooling2D(pool_size=2,
                              previous_layer=activation_layer2,
                              stride=1)
# drop_layer_2 = DropOutClass(previous_layer=max_pooling_layer, percentage=0.25)

print("\t3s conv layer ")
conv_layer3 = Conv2D(num_filters=1,
                     kernel_size=3,
                     previous_layer=max_pooling_layer,
                     activation_function=None)
relu_layer3 = ActivationFunction(previous_layer=conv_layer3)
pooling_layer = Pooling2D(pool_size=2,
                          previous_layer=relu_layer3,
                          stride=2 )

flatten_layer = Flatten(previous_layer=pooling_layer)

print("**  FOURTH_STEP ==> FULLY_CONNECTED LAYER **")
dense_layer1 = Dense(num_neurons=256,
                     previous_layer=flatten_layer,
                     activation_function="relu")
# drop_layer_3 = DropOutClass(previous_layer=dense_layer1, percentage=0.5)

dense_layer2 = Dense(num_neurons=num_classes,
                     previous_layer=dense_layer1,
                     activation_function="softmax")

model = Model(last_layer=dense_layer2,
              epochs=10,
              learning_rate=0.01)

model.train(train_inputs=input_layer.get_train_inputs(),
            train_outputs=input_layer.get_train_outputs())
print("train done!")
print("Prediction starts")
predictions = model.predict(data_inputs=input_layer.get_test_inputs())
# print(predictions)

num_wrong = numpy.where(predictions != input_layer.get_test_outputs())[0]
num_correct = input_layer.get_test_outputs().size - num_wrong.size
accuracy = 100 * (num_correct / input_layer.get_test_outputs().size)
print("Number of correct classifications : {num_correct}.".format(num_correct=num_correct))
print("Number of wrong classifications : {num_wrong}.".format(num_wrong=num_wrong.size))
print("Classification accuracy : {accuracy}.".format(accuracy=accuracy))

now2 = datetime.now()

current_time2 = now2.strftime("%H:%M:%S")

print("Current Time is :", current_time2)
print("duration Time is :", str((now2 - now)))
