# Implementing Convolutional Neural Networks
Python implementation for convolutional neural networks using NumPy.

This project can be used for classification problems where only 1 class per sample is allowed.
There is **no learning algorithm used**. We use **learning rate** to make some changes to the weights after each epoch.

**My Python version is 3.8.10 on M1 mac (64-bit)
NumPy version used is 1.22.0**

# Example

Check the [PyGAD's documentation](https://pygad.readthedocs.io/en/latest/README_pygad_cnn_ReadTheDocs.html) for information about the implementation of this example.

```python
import numpy

from training_helpers.input_2d import Input2D
from training_helpers.convolution_2d import Conv2D
from training_helpers.pooling_2d import Pooling2D
from training_helpers.flatten import Flatten
from training_helpers.activation_functions import ActivationFunction
from training_helpers.cnn_model import Model
from training_helpers.dense import Dense

num_classes = 4

input_layer = Input2D()

print("\n**  SECOND_STEP ==> CONV LAYER **")
print("\t1s conv layer")
conv_layer1 = Conv2D(num_filters=2,
                     kernel_size=3,
                     previous_layer=input_layer,
                     activation_function=None)

activation_layer1 = ActivationFunction(previous_layer=conv_layer1)
average_pooling_layer = Pooling2D(pool_size=2,
                                  previous_layer=activation_layer1,
                                  stride=2)
print("\t2s conv layer")
conv_layer2 = Conv2D(num_filters=3,
                     kernel_size=3,
                     previous_layer=average_pooling_layer,
                     activation_function=None)
activation_layer2 = ActivationFunction(previous_layer=conv_layer2)
max_pooling_layer = Pooling2D(pool_size=2,
                              previous_layer=activation_layer2,
                              stride=2)
print("\t3s conv layer ")
conv_layer3 = Conv2D(num_filters=1,
                     kernel_size=3,
                     previous_layer=max_pooling_layer,
                     activation_function=None)
relu_layer3 = ActivationFunction(previous_layer=conv_layer3)
pooling_layer = Pooling2D(pool_size=2,
                          previous_layer=relu_layer3,
                          stride=2)

flatten_layer = Flatten(previous_layer=pooling_layer)

print("**  FOURTH_STEP ==> FULLY_CONNECTED LAYER **")
dense_layer1 = Dense(num_neurons=80,
                     previous_layer=flatten_layer,
                     activation_function="relu")
dense_layer2 = Dense(num_neurons=num_classes,
                     previous_layer=dense_layer1,
                     activation_function="softmax")

model = Model(last_layer=dense_layer2,
              epochs=1,
              learning_rate=0.01)

model.train(train_inputs=input_layer.get_train_inputs(),
            train_outputs=input_layer.get_train_outputs())

predictions = model.predict(data_inputs=input_layer.get_train_inputs())
# print(predictions)

num_wrong = numpy.where(predictions != input_layer.get_train_outputs())[0]
num_correct = input_layer.get_train_outputs().size - num_wrong.size
accuracy = 100 * (num_correct / input_layer.get_train_outputs().size)
print("Number of correct classifications : {num_correct}.".format(num_correct=num_correct))
print("Number of wrong classifications : {num_wrong}.".format(num_wrong=num_wrong.size))
print("Classification accuracy : {accuracy}.".format(accuracy=accuracy))
```

# For More Information

There are different resources that can be used to get started with the building CNN and its Python implementation. 

## Tutorial: Basic example is in basic_cnn folder

# Contact Us

For more info:<br>
LinkedIn: https://www.linkedin.com/in/nazanin-fereydooni-zade-ba4178151<br>
Email: nazanin.fereydoonizade@gmail.com<br>
