import sys

import numpy


class Convolution:

    def __init__(self, conv_filter, img):
        self._conv_filter = conv_filter
        self._img = img
        self._total_convolve()

    def _check_integrity_error(self, conv_filter_shape, img_shape):
        if len(img_shape) != len(conv_filter_shape) - 1:  # Check whether number of dimensions is the same
            print("Error: Number of dimensions in conv filter and image do not match.")
            exit()
        if len(img_shape) > 2 or len(
                conv_filter_shape) > 3:  # Check if number of image channels matches the filter depth.
            if self._img.shape[-1] != conv_filter_shape[-1]:
                print("Error: Number of channels in both image and filter must match.")
                sys.exit()
        if conv_filter_shape[1] != conv_filter_shape[2]:  # Check if filter dimensions are equal.
            print('Error: Filter must be a square matrix. I.e. number of rows and columns must match.')
            sys.exit()
        if conv_filter_shape[1] % 2 == 0:  # Check if filter diemnsions are odd.
            print('Error: Filter must have an odd size. I.e. number of rows and columns must be odd.')
            sys.exit()

    @staticmethod
    def _single_convoltion(img, conv_filter):
        filter_size = conv_filter.shape[1]
        result = numpy.zeros((img.shape))
        # Looping through the image to apply the convolution operation.
        for r in numpy.uint16(numpy.arange(filter_size / 2.0,
                                           img.shape[0] - filter_size / 2.0 + 1)):
            for c in numpy.uint16(numpy.arange(filter_size / 2.0,
                                               img.shape[1] - filter_size / 2.0 + 1)):
                """
                Getting the current region to get multiplied with the filter.
                How to loop through the image and get the region based on 
                the image and filer sizes is the most tricky part of convolution.
                """
                curr_region = img[r - numpy.uint16(numpy.floor(filter_size / 2.0)):r + numpy.uint16(
                    numpy.ceil(filter_size / 2.0)),
                              c - numpy.uint16(numpy.floor(filter_size / 2.0)):c + numpy.uint16(
                                  numpy.ceil(filter_size / 2.0))]
                # Element-wise multipliplication between the current region and the filter.
                curr_result = curr_region * conv_filter
                conv_sum = numpy.sum(curr_result)  # Summing the result of multiplication.
                result[r, c] = conv_sum  # Saving the summation in the convolution layer feature map.

        # Clipping the outliers of the result matrix.
        final_result = result[numpy.uint16(filter_size / 2.0):result.shape[0] - numpy.uint16(filter_size / 2.0),
                       numpy.uint16(filter_size / 2.0):result.shape[1] - numpy.uint16(filter_size / 2.0)]
        return final_result

    @staticmethod
    def _create_empty_feature_map(conv_filter_shape, img_shape):
        return numpy.zeros((img_shape[0] - conv_filter_shape[1] + 1,
                            img_shape[1] - conv_filter_shape[1] + 1,
                            conv_filter_shape[0]))

    def _total_convolve(self):

        conv_filter_shape = self._conv_filter.shape
        img_shape = self._img.shape

        self._check_integrity_error(conv_filter_shape, img_shape)

        # Create An empty feature map to hold the output of convolving the filter(s) with the image.
        self.feature_map = self._create_empty_feature_map(conv_filter_shape, img_shape)

        # Convolving the image by the filter(s).
        for filter_num in range(conv_filter_shape[0]):
            current_filter = self._conv_filter[filter_num, :]

            if len(current_filter.shape) > 2:
                conv_map = self._single_convoltion(self._img[:, :, 0],
                                                   current_filter[:, :,
                                                   0])  # Array holding the sum of all feature maps.
                for ch_num in range(1, current_filter.shape[
                    -1]):  # Convolving each channel with the image and summing the results.
                    conv_map = conv_map + self._single_convoltion(self._img[:, :, ch_num],
                                                                  current_filter[:, :, ch_num])
            else:  # There is just a single channel in the filter.
                conv_map = self._single_convoltion(self._img, current_filter)
            self.feature_map[:, :, filter_num] = conv_map  # Holding feature map with the current filter.
            print("\t\tFilter",
                  str(filter_num + 1) + " convolved ==> Feature map shape is : " + str(self.feature_map.shape))
