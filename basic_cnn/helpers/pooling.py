import numpy


class Pooling:
    def __init__(self, feature_map):
        self._feature_map = feature_map

    def max_pool(self, size=2, stride=2):
        # Preparing the output of the pooling operation.
        pool_out = numpy.zeros((numpy.uint16((self._feature_map.shape[0] - size + 1) / stride + 1),
                                numpy.uint16((self._feature_map.shape[1] - size + 1) / stride + 1),
                                self._feature_map.shape[-1]))
        for map_num in range(self._feature_map.shape[-1]):
            r2 = 0
            for r in numpy.arange(0, self._feature_map.shape[0] - size + 1, stride):
                c2 = 0
                for c in numpy.arange(0, self._feature_map.shape[1] - size + 1, stride):
                    pool_out[r2, c2, map_num] = numpy.max([self._feature_map[r:r + size, c:c + size, map_num]])
                    c2 = c2 + 1
                r2 = r2 + 1
        print("\t\t\tPooling applied on feature map ==> shape is :" + str(pool_out.shape))
        return pool_out

    def min_pool(self, size=2, stride=2):
        # Preparing the output of the pooling operation.
        pool_out = numpy.zeros((numpy.uint16((self._feature_map.shape[0] - size + 1) / stride + 1),
                                numpy.uint16((self._feature_map.shape[1] - size + 1) / stride + 1),
                                self._feature_map.shape[-1]))
        for map_num in range(self._feature_map.shape[-1]):
            r2 = 0
            for r in numpy.arange(0, self._feature_map.shape[0] - size + 1, stride):
                c2 = 0
                for c in numpy.arange(0, self._feature_map.shape[1] - size + 1, stride):
                    pool_out[r2, c2, map_num] = numpy.min([self._feature_map[r:r + size, c:c + size, map_num]])
                    c2 = c2 + 1
                r2 = r2 + 1
        return pool_out
