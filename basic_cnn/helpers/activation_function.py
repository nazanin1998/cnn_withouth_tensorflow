import numpy


class ActivationFunction:
    def __init__(self, feature_map):
        self._feature_map = feature_map

    def relu(self):
        # Preparing the output of the ReLU activation function.
        print("\t\t\tReLU applied on feature map")
        map_shape = self._feature_map.shape
        relu_out = numpy.zeros(map_shape)
        for map_num in range(map_shape[-1]):
            for r in numpy.arange(0, map_shape[0]):
                for c in numpy.arange(0, map_shape[1]):
                    relu_out[r, c, map_num] = numpy.max([self._feature_map[r, c, map_num], 0])
        return relu_out
