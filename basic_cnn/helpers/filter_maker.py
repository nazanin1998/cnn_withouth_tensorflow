import numpy


class FilterMaker:

    @staticmethod
    def make_constant_filter():
        generated_filter = numpy.array([[[-1, 0, 1],
                                         [-1, 0, 1],
                                         [-1, 0, 1]], [[1, 1, 1],
                                                       [0, 0, 0],
                                                       [-1, -1, -1]]])
        print("\tMaking constant filters ==> Filer shape is : " + str(generated_filter.shape))
        return generated_filter

    @staticmethod
    def make_random_filter(first, second, third, fourth):
        generated_filter = numpy.random.rand(first, second, third, fourth)
        print("\tMaking random filters ==> Filer shape is : " + str(generated_filter.shape))
        return generated_filter
