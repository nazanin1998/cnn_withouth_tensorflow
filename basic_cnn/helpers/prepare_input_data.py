import skimage


# Preparing input of Conv

class PrepareInputDate:

    @staticmethod
    def read_input_image_from_skimage():  # Reading the input image that uploaded in project folder.
        img = skimage.data.chelsea()
        print("**  FIRST_STEP ==> Preparing input of Conv: **\n\tRead image with shape : " + str(img.shape))
        return img

    @staticmethod
    def read_input_image_from_io(image_name):  # imageName like "test.jpg"
        img = skimage.io.imread(image_name)
        return img

    @staticmethod
    def read_input_image_camera():
        img = skimage.data.camera()
        return img

    @staticmethod
    def covert_image_into_gray(image):
        img = skimage.color.rgb2gray(image)
        print("\tConvert into gray with shape : " + str(img.shape))
        return img
