# Basic Implementation of Convolutional Neural Network

In this Project, you can see basic implementation of Convolutional neural network using NumPy. There are three layers
which are convolution, ReLU, and max pooling. Main Flow of this repository is as follows:

1. Preparing input of Conv: Reading the input image that uploaded in project folder.
2. Preparing input of Conv: Making filters.
3. Convolution layer: Mapping each filter on the receptive fields of input image.
4. Output of Conv: (ReLU layer) Applying ReLU activation function on the feature maps.
5. Max Pooling layer: Applying the pooling operation on the output of ReLU layer.
6. Stacking conv, ReLU, and max pooling layers

**My Python version is 3.8.10 on M1 mac (64-bit)
NumPy version used is 1.22.0**

If you want test this project, the only thing you need to do is run file named **execute_project.py**.</br>
First of all, you should prepare data for first conv layer:
So it's time to prepare filters, and it depends on input image dimensions. So, Because our image is gray scale, then the
filter just have two dimension (height, with, and no depth). The filter created below specify three numbers (number of
filters, width, and height). Our example create two 3x3 filters by **make_constant_filter** method.

```python
# FIRST_STEP ==> Preparing input of Conv: 
# Read image from skimage and Convert image into gray 
# Then make first layer filters

rgb_input_image_matrix = PrepareInputDate.read_input_image_from_skimage()
gray_image_matrix = PrepareInputDate.covert_image_into_gray(rgb_input_image_matrix)
first_layer_filter = FilterMaker.make_constant_filter()
```

Next is to apply the filters on input image. First by **convolution.py** do convolution part, then use **relu** method
to apply ReLU activation function on conv output, and then use **max_pool**. Repeat this process for three (what ever
you need) times again.

```python
# SECOND_STEP ==> Multiple Convolution Layer
# First conv layer
print("\n**  SECOND_STEP ==> Convolution layer **\n\tFirst Convolutional Layer Started")
l1_feature_map = Convolution(first_layer_filter, gray_image_matrix).feature_map
l1_feature_map_relu = ActivationFunction(l1_feature_map).relu()
l1_feature_map_relu_pool = Pooling(l1_feature_map_relu).max_pool(size=2, stride=2)
print("\tEnd of first convolutional layer\n")

# Second conv layer
print("\tSECOND Convolutional Layer Started")
second_layer_filter = FilterMaker.make_random_filter(3, 5, 5, l1_feature_map_relu_pool.shape[-1])
l2_feature_map = Convolution(second_layer_filter, l1_feature_map_relu_pool).feature_map
l2_feature_map_relu = ActivationFunction(l2_feature_map).relu()
l2_feature_map_relu_pool = Pooling(l2_feature_map_relu).max_pool(size=2, stride=2)
print("\tEnd of second convolutional layer\n")

# Third conv layer
print("\tTHIRD Convolutional Layer Started")
third_layer_filter = FilterMaker.make_random_filter(1, 7, 7, l2_feature_map_relu_pool.shape[-1])
l3_feature_map = Convolution(third_layer_filter, l2_feature_map_relu_pool).feature_map
l3_feature_map_relu = ActivationFunction(l3_feature_map).relu()
l3_feature_map_relu_pool = Pooling(l3_feature_map_relu).max_pool(size=2, stride=2)
print("\tEnd of third convolutional layer\n")


```

Here you see the outputs of first conv-relu-pool layers.</br>
![l1](https://gitlab.com/nazanin1998/cnn_withouth_tensorflow/-/raw/master/example_project/L1.png)


Here you see the outputs of second conv-relu-pool layers.</br>
![l2](https://gitlab.com/nazanin1998/cnn_withouth_tensorflow/-/raw/master/example_project/L2.png)


Here you see the outputs of third conv-relu-pool layers.</br>
![l3](https://gitlab.com/nazanin1998/cnn_withouth_tensorflow/-/raw/master/example_project/L3.png)

For more info:<br>
LinkedIn: https://www.linkedin.com/in/nazanin-fereydooni-zade-ba4178151<br>
Email: nazanin.fereydoonizade@gmail.com<br>
